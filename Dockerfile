FROM nginx:latest
FROM php:7.4

COPY index.html/usr/share/nginx/html
RUN apt-get update && apt-get install -y unzip
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install system dependencies for JMeter and PHP extensions
RUN apt-get update && apt-get install -y default-jdk wget unzip \
    && wget -q -O /tmp/jmeter.zip https://dlcdn.apache.org//jmeter/source/apache-jmeter-5.6.3_src.zip \
    && unzip -o /tmp/jmeter.zip -d /opt/ \
    && ln -s /opt/apache-jmeter-5.4.1/bin/jmeter /usr/bin/jmeter

# Install PHP extensions if needed
RUN docker-php-ext-install pdo_mysql